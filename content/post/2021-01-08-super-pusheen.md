---
title: Super Pusheen
subtitle: Play Super Pusheen Game!
date: 2021-01-08
tags: ["super mario", "game", "programming"]
---

This is a copycat of **Super Mario Brothers** by Nintendo.
Built from scratch, this resembles the original game yet you are Pusheen as a player!  
[Go to the project](https://github.com/hryungk/SuperPusheen) 

{{< gallery caption-effect="fade" >}}
  {{< figure src="/coding/images/super-pusheen/1-Title_menu.png" link="/images/super-pusheen/1-Title_menu.png" caption="Title menu" alt="The title menu on the initial screen." >}}
  {{< figure src="/coding/images/super-pusheen/Fire_Pusheen_starman.png" link="/images/super-pusheen/Fire_Pusheen_starman.png" caption="Game play" alt="A demonstration of the game play showing mushroom, flower, and starman effect." >}}
  {{< figure src="/coding/images/super-pusheen/9-Ending.png" link="/images/super-pusheen/9-Ending.png" caption="Ending" alt="The ending screen of the game." >}}
{{< /gallery >}}
{{< load-photoswipe >}}