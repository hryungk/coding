---
title: About me
subtitle: Hyunryung Kim
comments: false
---

I understand my first name can be challenging.. For that I go by **Helen** most times.  
But how will you find me with Helen Kim when there are hundreds of them?  
So I decided to stick to my formal name for this website.  


### My history

I was born and raised in South Korea. I moved to the US for graduate school in 2014. While my research was focused in mechanical/aerospace engineering, I found myself growing interest in programming. I started studying coding in Java after I quit my last job in 2019. This website is dedicated to showcase my personal coding projects. I hope you enjoy them all :)