---
title: Nonogram Solver
subtitle: Solving a Nonogram puzzle
date: 2021-01-07
tags: ["nonogram", "programming"]
---

This program solves a number of [Nonogram](https://www.nonograms.org) puzzles. Nonogram is a Japanese crosswords puzzle that creates a graphic when solved.   

{{< gallery caption-effect="fade" >}}
  {{< figure src="/coding/images/nonogram-solver/2-1-2-Choose_a_puzzle_number_shadow.png" link="/images/nonogram-solver/2-1-2-Choose_a_puzzle_number_shadow.png" caption="Choose a puzzle" alt="A dialog box to choose puzzle's size and number." >}}
  {{< figure src="/coding/images/nonogram-solver/3-Solving-2.png" link="/images/nonogram-solver/3-Solving-2.png" caption="Display solving progress" alt="A visualization of the puzzle and solution progress." >}}
  {{< figure src="/coding/images/nonogram-solver/Output_screenshot.png" link="/images/nonogram-solver/Output_screenshot.png" caption="Text output" alt="A screenshot of the text output in Java. It describes how the program solves the puzzle." >}}
{{< /gallery >}}
{{< load-photoswipe >}}


This program lets a user to choose either a specific or random puzzle. When choosing a specific puzzle, the user is prompted to select a size of the puzzle and a puzzle number. These numbers are assigned to puzzles without any priority.  
Once a puzzle is selected, the program starts to solve the puzzle. The dialog box shows the progression of solution.   
In the text output section, you will see how exactly the puzzle is being solved.   



The design of the visualization of the puzzle is inspired by [Nonogram.com](https://Nonogram.com).  
If you are interested in looking into the project, click the link below:  
[Go to the project](https://github.com/hryungk/NonogramSolver)
