---
title: What Goes Where in Redmond?
subtitle: Find out how to best dispose of stuff in Redmond, WA
date: 2021-10-17
tags: ["Java web application", "sustainability", "programming"]
---

This is a Java Web Application project that I worked on during the coding boot camp at [Per Scholas](https://perscholas.org/courses/full-stack-java-developer/full-stack-java-developer-powered-by-teksystems-seattle/). It is a crowd-knowledge-based database to search how to dispose of items properly in Redmond, WA, USA. Once you create an account, you can contribute to the database to search on.

[Go to the project](https://github.com/hryungk/WhatGoesWhere) 

{{< gallery caption-effect="fade" >}}
  {{< figure src="/coding/images/what-goes-where-in-redmond/1-main_page.png" link="/images/what-goes-where-in-redmond/1-main_page.png" caption="Main page" alt="The main page of the website." >}}
  {{< figure src="/coding/images/what-goes-where-in-redmond/2-search_result.png" link="/images/what-goes-where-in-redmond/2-search_result.png" caption="Search result" alt="A page of search results for an item on the main page." >}}
  {{< figure src="/coding/images/what-goes-where-in-redmond/3-profile_page.png" link="/images/what-goes-where-in-redmond/3-profile_page.png" caption="Profile page" alt="The profile page after logged in." >}}
{{< /gallery >}}
{{< load-photoswipe >}}